
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_RT_DEVICE_CLASS_H_
#define _FesaTestClass_RT_DEVICE_CLASS_H_

#include <FesaTestClass/GeneratedCode/RTDeviceClassGen.h>

namespace FesaTestClass
{

class RTDeviceClass: public RTDeviceClassGen
{
public:
    static RTDeviceClass* getInstance();
    static void releaseInstance();
    void specificInit();
    void specificShutDown();
private:
    RTDeviceClass();
    virtual ~RTDeviceClass();
    static RTDeviceClass* instance_;
};

} // FesaTestClass

#endif // _FesaTestClass_RT_DEVICE_CLASS_H_
