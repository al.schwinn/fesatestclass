
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_StatusUpdateAction_H_
#define _FesaTestClass_StatusUpdateAction_H_

#include <FesaTestClass/GeneratedCode/Device.h>
#include <FesaTestClass/GeneratedCode/GenRTActions.h>

namespace FesaTestClass
{

class StatusUpdateAction : public StatusUpdateActionBase
{
public:
    StatusUpdateAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~StatusUpdateAction();
    void execute(fesa::RTEvent* pEvt);
};

} // FesaTestClass

#endif // _FesaTestClass_StatusUpdateAction_H_
