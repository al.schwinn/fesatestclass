
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FesaTestClass/GeneratedCode/ServiceLocator.h>
#include <FesaTestClass/RealTime/TimingAction.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FesaTestClass.RealTime.TimingAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FesaTestClass"; \
    diagMsg.name = "TimingAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FesaTestClassServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FesaTestClass
{

TimingAction::TimingAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     TimingActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

TimingAction::~TimingAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void TimingAction::execute(fesa::RTEvent* pEvt)
{
    static int value = 1;
    value += 2;
    std::cout << "TimingAction::execute" << std::endl;

    const fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    static_cast<void>(context); // This line prevents an "unused variable" warning, it can be removed safely.
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device* device = *it;
            device->muxedField.set(value, context);
            device->acquisitionContext.insert(pEvt->getMultiplexingContext());
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // FesaTestClass
