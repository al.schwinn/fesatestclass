
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FesaTestClass/GeneratedCode/ServiceLocator.h>
#include <FesaTestClass/RealTime/StatusUpdateAction.h>
#include <fesa-core/Synchronization/NoneContext.h>

#include <cmw-log/Logger.h>
#include <random>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FesaTestClass.RealTime.StatusUpdateAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FesaTestClass"; \
    diagMsg.name = "StatusUpdateAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FesaTestClassServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FesaTestClass
{

StatusUpdateAction::StatusUpdateAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     StatusUpdateActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

StatusUpdateAction::~StatusUpdateAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void StatusUpdateAction::execute(fesa::RTEvent* pEvt)
{
    static int value = 0;
    value +=2;
    std::cout << "StatusUpdateAction::execute" << std::endl;
    fesa::NoneContext noneContext;

    const fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    static_cast<void>(context); // This line prevents an "unused variable" warning, it can be removed safely.
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device* device = *it;
            device->nonMuxedField.set(value, context);
           // device->acquisitionContext.insert(pEvt->getMultiplexingContext());
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // FesaTestClass
