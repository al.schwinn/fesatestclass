
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_AcquisitionGetAction_H_
#define _FesaTestClass_AcquisitionGetAction_H_

#include <FesaTestClass/GeneratedCode/GetSetDefaultServerAction.h>
#include <FesaTestClass/GeneratedCode/PropertyData.h>
#include <FesaTestClass/GeneratedCode/TypeDefinition.h>
#include <FesaTestClass/GeneratedCode/Device.h>

namespace FesaTestClass
{

class AcquisitionGetAction : public AcquisitionGetActionBase
{
public:
    AcquisitionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~AcquisitionGetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data, const AcquisitionFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const;
};

} // FesaTestClass

#endif // _FesaTestClass_AcquisitionGetAction_H_
