
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FesaTestClass/Server/AcquisitionGetAction.h>
#include <FesaTestClass/GeneratedCode/ServiceLocator.h>

#include <fesa-core/Synchronization/NoneContext.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FesaTestClass.Server.AcquisitionGetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FesaTestClass"; \
    diagMsg.name = "AcquisitionGetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FesaTestClassServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FesaTestClass
{

AcquisitionGetAction::AcquisitionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        AcquisitionGetActionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

AcquisitionGetAction::~AcquisitionGetAction()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void AcquisitionGetAction::execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data, const AcquisitionFilterData& filter)
{
    auto muxType = pEvt->getMultiplexingContext()->getType();
    if (muxType == fesa::MultiplexingContext::NoneCtxt)
    {
        data.setField(pDev->nonMuxedField.get(pEvt->getMultiplexingContext()));

//        data.setProcessIndex(-1);
//        data.setSequenceIndex(-1);
//        data.setChainIndex(-1);
//        data.setEventNumber(-1);
//        data.setTimingGroupID(-1);
//        data.setEventStamp(-1);
//        data.setProcessStartStamp(-1);
//        data.setSequenceStartStamp(-1);
//        data.setChainStartStamp(-1);
    }
    else
    {
        data.setField(pDev->muxedField.get(pEvt->getMultiplexingContext()));

//        data.setProcessIndex(acquisitionContext->getProcessIndex());
//        data.setSequenceIndex(timing_context->getWRContext()->getSequenceIndex());
//        data.setChainIndex(timing_context->getWRContext()->getBeamProductionChainIndex());
//        data.setEventNumber(timing_context->getWRContext()->getEventNumber());
//        data.setTimingGroupID(timing_context->getWRContext()->getGroupID());
//        data.setEventStamp(timing_context->getWRContext()->getEventExecutionTimeStamp());
//        data.setProcessStartStamp(timing_context->getWRContext()->getProcessTimeStamp());
//        data.setSequenceStartStamp(timing_context->getWRContext()->getSequenceTimeStamp());
    }
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool AcquisitionGetAction::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool AcquisitionGetAction::isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // FesaTestClass
