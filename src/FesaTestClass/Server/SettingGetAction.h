
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_SettingGetAction_H_
#define _FesaTestClass_SettingGetAction_H_

#include <FesaTestClass/GeneratedCode/GetSetDefaultServerAction.h>
#include <FesaTestClass/GeneratedCode/PropertyData.h>
#include <FesaTestClass/GeneratedCode/TypeDefinition.h>
#include <FesaTestClass/GeneratedCode/Device.h>

namespace FesaTestClass
{

class SettingGetAction : public SettingGetActionBase
{
public:
    SettingGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SettingGetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, SettingPropertyData& data, const SettingFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const SettingFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const SettingFilterData& filter) const;
};

} // FesaTestClass

#endif // _FesaTestClass_SettingGetAction_H_
