
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_SettingSetAction_H_
#define _FesaTestClass_SettingSetAction_H_

#include <FesaTestClass/GeneratedCode/GetSetDefaultServerAction.h>
#include <FesaTestClass/GeneratedCode/PropertyData.h>
#include <FesaTestClass/GeneratedCode/TypeDefinition.h>
#include <FesaTestClass/GeneratedCode/Device.h>

namespace FesaTestClass
{

class SettingSetAction : public SettingSetActionBase
{
public:
    SettingSetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SettingSetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const SettingPropertyData& data, const SettingFilterData& filter);
};

} // FesaTestClass

#endif // _FesaTestClass_SettingSetAction_H_
