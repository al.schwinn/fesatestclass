
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FesaTestClass/Server/SettingSetAction.h>
#include <FesaTestClass/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FesaTestClass.Server.SettingSetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FesaTestClass"; \
    diagMsg.name = "SettingSetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FesaTestClassServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FesaTestClass
{

SettingSetAction::SettingSetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        SettingSetActionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SettingSetAction::~SettingSetAction()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void SettingSetAction::execute(fesa::RequestEvent* pEvt, Device* pDev, const SettingPropertyData& data, const SettingFilterData& filter)
{
    auto muxType = pEvt->getMultiplexingContext()->getType();
    if (muxType == fesa::MultiplexingContext::NoneCtxt)
    {
       std::cout << "None Context - This code cannot be reched, using an empty selector for a muxed setting property will trow: FESA_8023" << std::endl;

    }
    else
    {
        std::cout << "Timing Context" << std::endl;
    }
}

} // FesaTestClass
