
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FesaTestClass_SERVER_DEVICE_CLASS_H_
#define _FesaTestClass_SERVER_DEVICE_CLASS_H_

#include <FesaTestClass/GeneratedCode/ServerDeviceClassGen.h>

namespace FesaTestClass
{

class ServerDeviceClass: public ServerDeviceClassGen
{
    public:
        static ServerDeviceClass* getInstance();
        static void releaseInstance();
        void specificInit();
        void specificShutDown();
    private:
        ServerDeviceClass();
        virtual  ~ServerDeviceClass();
        static ServerDeviceClass* instance_;
};

} // FesaTestClass

#endif // _FesaTestClass_SERVER_DEVICE_CLASS_H_
